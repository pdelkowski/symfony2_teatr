<?php

namespace Codersmill\ArchiveBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Codersmill\ArchiveBundle\Form\Type\ContactType;
/**
 * Contact controller.
 *
 */
class ContactController extends BaseController
{
    public function indexAction(Request $request)
    {
        $form = $this->createForm(new ContactType());

        if ($request->isMethod('POST')) {
            $form->bind($request);

            if ($form->isValid()) {
                $message = \Swift_Message::newInstance()
                    ->setSubject($form->get('subject')->getData())
                    ->setFrom($form->get('email')->getData())
                    ->setTo('contact@example.com')
                    ->setBody(
                        $this->renderView(
                            'CodersmillArchiveBundle:Contact:mail.html.twig',
                            array(
                                'ip' => $request->getClientIp(),
                                'name' => $form->get('name')->getData(),
                                'message' => $form->get('message')->getData()
                            )
                        )
                    );

                $this->get('mailer')->send($message);

                $request->getSession()->getFlashBag()->add('success', 'Twoja wiadomość została wysłana. Dziękujemy!');

                return $this->redirect($this->generateUrl('codersmill_archive_contact_sent'));
            }
        }

        return $this->render('CodersmillArchiveBundle:Contact:contact.html.twig', array('form' => $form->createView()));
    }

    public function sentAction()
    {
        return $this->render('CodersmillArchiveBundle:Contact:success.html.twig');
    }
}
