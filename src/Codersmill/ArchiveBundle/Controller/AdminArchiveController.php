<?php

namespace Codersmill\ArchiveBundle\Controller;

use Codersmill\ArchiveBundle\Entity\Import;
use Codersmill\ArchiveBundle\Entity\Archive;
use Codersmill\ArchiveBundle\Entity\Review;
use Codersmill\ArchiveBundle\Entity\Photography;
use Codersmill\ArchiveBundle\Entity\Drawing;
use Codersmill\ArchiveBundle\Entity\Memory;
use Codersmill\ArchiveBundle\Entity\Document;
use Doctrine\Common\Collections\ArrayCollection;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Finder\Finder;
use Codersmill\ArchiveBundle\Entity\Poetry;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery as ProxyQueryInterface;
use \Imagine\Gd\Imagine;
use Imagine\Image\Box;

class AdminArchiveController extends CRUDController
{
    public function getTemplate($name)
    {
        switch ($name) {
            case 'import':
                return 'ApplicationSonataUserBundle:CRUD:import.html.twig';
                break;
            case 'import_select_detail':
                return 'ApplicationSonataUserBundle:CRUD:import_detail.html.twig';
                break;
            case 'import_select_records':
                return 'ApplicationSonataUserBundle:CRUD:import_records.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function importAction(Request $request)
    {
//        $format = $request->get('format');
//
//        $allowedExportFormats = array('xls');
//
//        if (!in_array($format, $allowedExportFormats)) {
//            throw new \RuntimeException(sprintf('Import in format `%s` is not allowed for class: `%s`. Allowed formats are: `%s`', $format, $this->admin->getClass(), implode(', ', $allowedExportFormats)));
//        }

        // the key used to lookup the template
        $templateKey = 'import';

        $object = $this->admin->getNewInstance();
        $this->admin->setSubject($object);
        $adminCode = $this->container->get('request')->get('_sonata_admin');

        if (!$adminCode) {
            throw new \RuntimeException(sprintf('There is no `_sonata_admin` defined for the controller `%s` and the current route `%s`', get_class($this), $this->container->get('request')->get('_route')));
        }

        $admin = $this->container->get('sonata.admin.pool')->getAdminByAdminCode($adminCode);


        $import = new Import();
        $form = $this->createFormBuilder($import);
        $form->add('file', 'file', array('label' => 'Plik Excel (xls)', 'required' => true));
        $form->add('save', 'submit', array('label' => 'Załaduj', 'attr' => array('class' => 'btn btn-small btn-primary')));

        $form->setAction($admin->generateUrl('import'));
        $form->setMethod('POST');
        $form = $form->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $templateKey = 'import_select_detail';
            $em = $this->getDoctrine()->getManager();

            $import->upload();

            $em->persist($import);
            $em->flush();


            try {
                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($import->getAbsolutePath());
            } catch( Exception $e) {
                throw new \RuntimeException('Error while loading XLS file');
            }

            $sheet_names = $phpExcelObject->getSheetNames();
            $sheet_names = array_combine($sheet_names, $sheet_names);
            $archive_types = Archive::getArchiveModelNames();

            $form_detail = $this->createFormBuilder();
            $form_detail->add('sheet', 'choice', array(
                'label'     => 'Wybierz arkusz',
                'choices'   => $sheet_names,
                'multiple'  => false,
                'expanded'  => false
            ));
            $form_detail->add('archive_type', 'choice', array(
                'label'     => 'Wybierz archiwum',
                'choices'   => $archive_types,
                'multiple'  => false,
                'expanded'  => false
            ));
            $form_detail->add('file_path', 'hidden', array(
                'data' => $import->getAbsolutePath()
            ));
            $form_detail->add('continue', 'submit', array('label' => 'Dalej', 'attr' => array('class' => 'btn btn-small btn-primary')));
            $form_detail->setAction($admin->generateUrl('importProcess'));
            $form_detail->setMethod('POST');
            $form_detail = $form_detail->getForm();
            $form_view = $form_detail->createView();

            return $this->render($this->getTemplate($templateKey), array(
                'sheet_names' => $sheet_names,
                'form'          => $form_view
            ));
        }


        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->getTemplate($templateKey), array(
            'action' => 'create',
            'form'   => $view,
            'object' => $object,
        ));

    }

    public function importProcessAction(Request $request)
    {
        $templateKey = 'import_select_records';

//        echo '<pre>';
        $postData = $request->get('form');

        try {
            $xls = $this->get('phpexcel')->createPHPExcelObject($postData['file_path']);
        } catch( Exception $e) {
            throw new \RuntimeException('Error while loading XLS file');
        }

        $sheet = $xls->getSheetByName($postData['sheet']);
        $records = $sheet->toArray();
        $fields = Archive::getArchiveFieldsByModelName($postData['archive_type']);

        // delete all empty cells (from xls)
        foreach($records as $key => $record)
            $records[$key] = array_slice($record, 0, count($fields));

        // delete unneeded records (headers etc)
//        unset($records[0]);
//        unset($records[1]);
//        unset($records[2]);

//        echo '<pre>';
////        var_dump($sheet->getRowIterator()->current()->getCellIterator()->current() );
//        var_dump(get_class_methods( $sheet ));
//        die;


        return $this->render($this->getTemplate($templateKey), array(
            'records'   => $records,
            'fields'    => $fields,
            'archive_class' => $postData['archive_type']
        ));

        // unset record if it has no scan_number
        foreach($records as $record_key => $record_value) {
            if($record_value[0] == NULL)
                unset($records[$record_key]);
        }

//        echo '<pre>';
//        var_dump($fields);
//        var_dump($records);
//        die;

        $em = $this->getDoctrine()->getManager();

        foreach($records as $record) {

            $record = $this->makeValidRecord($record);

            // Memory
//            $archive = new Memory();
//            $archive->setScanNumber($record[0]);
//            $archive->setFormat($record[1]);
//            $archive->setDateCreatedDigital($record[2]);
//            $archive->setDigitalCreator($record[3]);
//            $archive->setOwner($record[4]);
//            $archive->setDevice($record[5]);
//            $archive->setSignature($record[6]);
//            $archive->setTitle($record[7]);
//            $archive->setArchiveAuthor($record[8]);
//            $archive->setDescription($record[9]);
//            $archive->setPublishHouse($record[10]);
//            $archive->setPublishDate($record[11]);
//            $archive->setPublishLocation($record[12]);
//            $archive->setDateCreated($record[13]);
//            $archive->setStorageLocation($record[14]);
//            $archive->setDomain($record[15]);

            // Drawing
//            $archive = new Drawing();
//            $archive->setScanNumber($record[0]);
//            $archive->setFormat($record[1]);
//            $archive->setDateCreatedDigital($record[2]);
//            $archive->setDigitalCreator($record[3]);
//            $archive->setTitle($record[4]);
//            $archive->setDescription($record[5]);
//            $archive->setDateCreated($record[6]);
//            $archive->setSignature($record[7]);
//            $archive->setArchiveAuthor($record[8]);
//            $archive->setStorageLocation($record[9]);
//            $archive->setDomain($record[10]);
//            $archive->setOwner($record[11]);
//            $archive->setDevice($record[12]);

            // Review
            $archive = new Review();
            $archive->setScanNumber($record[0]);
            $archive->setFormat($record[8]);
            $archive->setDateCreatedDigital($record[9]);
            $archive->setDigitalCreator($record[10]);
            $archive->setTitle($record[5]);
            $archive->setDescription($record[6]);
            $archive->setDateCreated($record[7]);
//            $archive->setSignature($record[7]);
            $archive->setArchiveAuthor($record[4]);
//            $archive->setStorageLocation($record[9]);
//            $archive->setDomain($record[10]);
            $archive->setOwner($record[11]);
            $archive->setDevice($record[12]);

            $archive->setPlay($record[3]);
            $archive->setArtPlay($record[2]);
            $archive->setTheater($record[1]);

            // Photography
//            $archive = new Photography();
//            $archive->setScanNumber($record[0]);
//            $archive->setFormat($record[1]);
//            $archive->setDateCreatedDigital($record[2]);
//            $archive->setDigitalCreator($record[3]);
//            $archive->setDevice($record[4]);
//            $archive->setTitle($record[5]);
//            $archive->setDescription($record[6]);
//            $archive->setDateCreated($record[7]);
//            $archive->setArchiveAuthor($record[8]);
//            $archive->setStorageLocation($record[9]);
//            $archive->setSignature($record[10]);
//            $archive->setDomain($record[11]);

            // Poetry
//            $archive->setScanNumber($record[0]);
//            $archive->setTitle($record[1]);
//            $archive->setArchiveAuthor($record[2]);
//            $archive->setDescription($record[3]);
//            $archive->setDateCreated($record[4]);
//            $archive->setStorageLocation($record[5]);
//            $archive->setSignature($record[6]);
//            $archive->setDomain($record[7]);
//            $archive->setFormat($record[8]);
//            $archive->setDateCreatedDigital($record[9]);
//            $archive->setDigitalCreator($record[10]);
//            $archive->setOwner($record[11]);
//            $archive->setDevice($record[12]);


            $archive->setAccess('public');
            $archive->setSlug('url');
            $archive->setArchiveType('Recenzja');

            $em->persist($archive);
            $archive = NULL;

//            $em->flush();
//            echo 'poszlo';
//            die;
        }

//        $em->flush();


        echo 'poszlo wszystko';
        die;


//        echo $request->get('file_path');
//        $sheet = $xls->getSheetNames();
//        var_dump($request->get('form'));
//        print_r($fields);
//        var_dump(get_class_methods($sheet));

//        echo '<pre>';
//        $import = new Import();
//        $form = $this->createFormBuilder($import);
//        $form->add('file', 'file', array('label' => 'Plik Excel (xls)', 'required' => true));
//        $form->add('save', 'submit', array('label' => 'Załaduj', 'attr' => array('class' => 'btn btn-small btn-primary')));
//        $form = $form->getForm();
//        $form->handleRequest($request);
//
//        if ($form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//
//            $em->persist($import);
//            $em->flush();
//
//
//
//            echo 'Zapisano!'; die;
////            return $this->redirect($this->generateUrl(...));
//        } else {
////            var_dump(get_class_methods($form));
////            var_dump($form);
//            var_dump($form->getErrors()->current());
//        }
//
//        $xml_file = $request->files->get('form')['file']->getRealPath();
//
//        try {
//            $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($xml_file); // '/var/www/teatr.dev/src/Codersmill/ArchiveBundle/Resources/public/archive2.xls'
//            $xml = $phpExcelObject;
//            $archive_xls = $phpExcelObject;
//        } catch( Exception $e) {
//            print_r($e); die;
//        }
//
//        $sheet_names = $phpExcelObject->getSheetNames();
//
//        print_r($sheet_names); die;
//
////        $a = $phpExcelObject->getSheetByName('F - Fotografie');
////        var_dump($a->toArray()); die;
//
//
//        var_dump($request->files->get('form')['file']->getRealPath());
//        var_dump(get_class_methods($phpExcelObject));
//        var_dump(get_class_methods($this->get('request')->request));
        echo 'Im processing...';
        die;
    }


    public function importSaveAction(Request $request)
    {
        $r = $this->getRequest()->request->all();//$request->get('form');


        $fields = array_values($request->get('field'));




        $i_counter = 0;
        $no_field = false;

        if( count(array_unique($fields)) != count($fields) ) {
            $this->addFlash('sonata_flash_error', 'Wystąpił błąd podczas importowania archiwów. Kolumny muszą być unikatowe.');
            return new RedirectResponse( $this->admin->generateUrl('list',$this->admin->getFilterParameters()) );
        }


        $ids = array_values($request->get('ids'));
        $items = array_values($request->get('item'));
        $archive_model = $request->get('archive_class');

        $em = $this->getDoctrine()->getManager();

//        echo '<pre>';
//        var_dump($r); die;

        try {

            foreach($items as $item_key => $item)
            {
                if(!isset($ids[$item_key]) || $ids[$item_key] != "on")
                    continue;

                // create class
                $class_name = '\\Codersmill\\ArchiveBundle\\Entity\\'.$archive_model;
                $record = new $class_name;

                // setters
                foreach($fields as $key => $field)
                {
                    if($field == 'nofield')
                    {
                        $no_field = true;
                        break;
                    }

                    $method = $this->stringToMethodName($field);

                    if( $item[$key] == NULL )
                        $value = '';
                    else
                        $value = $item[$key];

                    $record->$method($value);
                }

                if($no_field) {
                    $no_field = false;
                    continue;
                }

                $record->setAccess('public');
                $record->setSlug('url');
                $archive_type_label = Archive::getArchiveModelNames();
                $record->setArchiveType($archive_type_label[$archive_model]);

                // add record to collection
                $em->persist($record);
                $record = NULL;
                $i_counter++;
            }

            $em->flush();

        } catch( Exception $e ) {
            $this->addFlash('sonata_flash_error', 'Wystąpił błąd podczas importowania archiwów.');
            return new RedirectResponse( $this->admin->generateUrl('list',$this->admin->getFilterParameters()) );
        }

        $this->addFlash('sonata_flash_success', 'Zaimportowano '.$i_counter.' pozycji.');

        return new RedirectResponse(
            $this->admin->generateUrl('list',$this->admin->getFilterParameters())
        );
    }


    public function batchActionGenerate(ProxyQueryInterface $selectedModelQuery)
    {
        if (!$this->admin->isGranted('DELETE'))
        {
            throw new AccessDeniedException();
        }

        $request = $this->get('request');
        $modelManager = $this->admin->getModelManager();

        $em = $this->getDoctrine()->getManager();

        try {

            foreach($selectedModelQuery->execute() as $selectedModel)
            {
                    $model_scan_numer = $selectedModel->getScanNumber();
                    $thumb = $this->createThumbFromScanNumber($model_scan_numer);
//                    $thumb = $this->createThumbFromScanNumber($selectedModel->getScanNumber());
                    if(!$thumb)
                    {
                           $this->addFlash('sonata_flash_error', 'Nie znaleziono odpowiadajacego skanu archiwa.');
                           return new RedirectResponse( $this->admin->generateUrl('list',$this->admin->getFilterParameters()) );
                    }
                    $selectedModel->setThumb($thumb);
                    $em->persist($selectedModel);
            }
            $em->flush();

        } catch (Exception $e) {

            $this->addFlash('sonata_flash_error', 'Wystąpił błąd podczas generowania obrazków.');

            return new RedirectResponse( $this->admin->generateUrl('list',$this->admin->getFilterParameters()) );
        }


        $this->addFlash('sonata_flash_success', 'Wygenerowano obrazki');

        return new RedirectResponse(
            $this->admin->generateUrl('list',$this->admin->getFilterParameters())
        );
    }

    public function batchActionGenerateAll(ProxyQueryInterface $selectedModelQuery)
    {
        $repository = $this->getDoctrine()->getRepository('CodersmillArchiveBundle:Archive');
        $archives = $repository->findAll();
        $em = $this->getDoctrine()->getManager();

        foreach($archives as $archive)
        {
            if( $archive->getThumb() == NULL || $archive->getThumb() == '' )
            {
                try {
                    $generated_thumb = $this->createThumbFromScanNumber($archive->getScanNumber());

                    if(!$generated_thumb)
                    {
                        $this->addFlash('sonata_flash_error', 'Nie znaleziono: '.$archive->getScanNumber().'<br>');
//                        return new RedirectResponse($this->admin->generateUrl('list',$this->admin->getFilterParameters()));
                    } else {
                        $archive->setThumb($generated_thumb);
                        $em->persist($archive);
                        $em->flush();
                    }

                } catch (Exception $e) {
                    $this->addFlash('sonata_flash_error', 'Wystąpił błąd podczas generowania wszystkich miniatur.');
                    return new RedirectResponse($this->admin->generateUrl('list',$this->admin->getFilterParameters()));
                }
            }
        }

        $this->addFlash('sonata_flash_success', 'Wygenerowano miniatury dla wszystkich');

        return new RedirectResponse($this->admin->generateUrl('list',$this->admin->getFilterParameters()));

        echo 'zakonczono poprawnie'; die;
    }

    protected function stringToMethodName($str)
    {
        return 'set'.preg_replace('/(?:^|_)(.?)/e',"strtoupper('$1')",$str); //str_replace(' ', '', ucwords(strtolower($str)));
    }

    protected function createThumbFromScanNumber($scan_number)
    {
        $finder = new Finder();
        $finder->files()->name($scan_number.'.*');
        $files = $finder->in($this->getArchiveRootDir());
        $new_file_dir = $this->getArchiveRootDir().'thumbs/';
        $watermark_file = $this->getArchiveRootDir().'../../images/watermark.png';
        $new_filepath = NULL;
        $new_filename = NULL;
        $file_name = NULL;

        foreach($files as $file)
            $file_name = $file->getPathName();

        if(!$file_name)
            return false;

        $new_filename = $scan_number.'.jpg';
        $new_filepath = $new_file_dir.$new_filename;


        $imagick = new \Imagick();
        $imagick->readImage($file_name);
        $imagick->thumbnailImage(800, 0);

        $watermark = new \Imagick();
        $watermark->readImage($watermark_file);

        // how big are the images?
        $iWidth = $imagick->getImageWidth();
        $iHeight = $imagick->getImageHeight();
        $wWidth = $watermark->getImageWidth();
        $wHeight = $watermark->getImageHeight();

        if ($iHeight < $wHeight || $iWidth < $wWidth) {
            // resize the watermark
            $watermark->scaleImage($iWidth, $iHeight);

            // get new size
            $wWidth = $watermark->getImageWidth();
            $wHeight = $watermark->getImageHeight();
        }

        // calculate the position
        $x = ($iWidth - $wWidth) / 1;
        $y = ($iHeight - $wHeight) / 1;

        $imagick->compositeImage($watermark, \imagick::COMPOSITE_OVER, $x, $y);

        $imagick->writeImage($new_filepath);

        if(!$new_filename)
            return false;

        return $new_filename;
    }

    protected function getArchiveRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/uploads/archives/';
    }

    protected function getModelFieldOrder()
    {
           return array(
               'scan_number', 'title', 'archive_author', 'description', 'date_created', 'storage_location',
               'signature', 'domain', 'format', 'date_created_digital', 'digital_creator', 'owner', 'device'
           );
    }

    protected function makeValidRecord($record)
    {
        foreach($record as $key => $value) {
            if($value == NULL)
                $record[$key] = '';
        }

        return $record;
    }
}