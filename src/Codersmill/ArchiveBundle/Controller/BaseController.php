<?php

namespace Codersmill\ArchiveBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response as Response;

class BaseController extends Controller
{
    /**
     * Overwrite default render method, attach tags data
     *
     * @param $template
     * @param $params
     */
    public function render($view, array $parameters = array(), Response $response = NULL)
    {
        $tag_repository = $this->getDoctrine()->getRepository('CodersmillArchiveBundle:Tag');
        $tags = $tag_repository->findAll();
        $tags = array('tags' => $tags);

        $params = array_merge($parameters, $tags);
        return parent::render($view, $params, $response);
    }
}