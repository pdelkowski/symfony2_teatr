<?php

namespace Codersmill\ArchiveBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Codersmill\ArchiveBundle\Entity\Archive;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends BaseController
{
    public function homepageAction()
    {
        $repository = $this->getDoctrine()->getRepository('CodersmillArchiveBundle:StaticPage');
        $page = $repository->findOneByLabel('Strona główna');

        return $this->render('CodersmillArchiveBundle:Default:homepage.html.twig', array('page' => $page));
    }


    public function indexAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('CodersmillArchiveBundle:Archive');
//        $archives = $repository->findAll();
        $archives = $repository->getAllPublicArchives();

        $archive_names = Archive::getArchiveModelNames();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $archives,
            $request->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );

//        try {
//            $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject('/var/www/teatr.dev/src/Codersmill/ArchiveBundle/Resources/public/archive2.xls');
//            $xml = $phpExcelObject;
//            $archive_xls = $phpExcelObject;
//        } catch( Exception $e) {
//            print_r($e); die;
//        }
//
//#        echo "<pre>";
//        $a = $phpExcelObject->getSheetByName('F - Fotografie');
#        var_dump($a->toArray());
#        print_r(get_class_methods($a));
#        var_dump($phpExcelObject->getSheetNames());
#        $sheet_count = $archive_xls->getSheetCount();
#        $sheet_names = $archive_xls->getSheetName();

#        print_r(get_class_methods($phpExcelObject));
#        var_dump($phpExcelObject);
#        die;
    
        return $this->render('CodersmillArchiveBundle:Default:index.html.twig', array('archives' => $archives, 'pagination' => $pagination, 'archive_names' => $archive_names));
    }
    
    public function showAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('CodersmillArchiveBundle:Archive');

        $archive = $repository->find($id);
        $latest = $repository->getLatestArchives();
        $tags = $archive->getTags();

        $categories =  $archive_names = Archive::getArchiveModelNames();

        return $this->render('CodersmillArchiveBundle:Default:show.html.twig', array('archive' => $archive, 'latests' => $latest, 'archive_tags' => $tags, 'categories' => $categories));
    }

    public function tagsAction(Request $request, $name)
    {
        $repository = $this->getDoctrine()->getRepository('CodersmillArchiveBundle:Tag');

        $query = $repository->createQueryBuilder('p')
            ->where('p.tag_name = :tagname')
            ->setParameter('tagname', $name)
            ->getQuery()
            ->getResult();

        $tags = $query[0];

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $tags->getArchives(),
            $request->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );

        $archive_names = Archive::getArchiveModelNames();

        return $this->render('CodersmillArchiveBundle:Default:index.html.twig', array('pagination' => $pagination, 'archive_names' => $archive_names));

        echo '<pre>';
        var_dump($query->getResult()[0]->getArchives()->toArray());
        die;

//        $query = $repository->createQueryBuilder('c')
//            ->innerJoin('c.archive','u')
//            ->where('c.company = :company')
//            ->setParameter('company', $companyId)
//            ->orderBy('c.name', 'ASC')
//            ->getQuery();
//        $containers = $query->getResult();
    }
}
