<?php

namespace Codersmill\ArchiveBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Codersmill\ArchiveBundle\Entity\Archive;

class SearchController extends BaseController
{
    public function indexAction(Request $request)
    {
        $archive_title  = $request->get('title');
        $archive_type   = $request->get('archive_type');
        $archive_date   = $request->get('date');
        $archive_author = $request->get('author');

        $params = array();

        if( $archive_title )
            $params['title'] = $archive_title;

        if( $archive_date )
            $params['date'] = $archive_date;

        if( $archive_author )
            $params['author'] = $archive_author;

        if( $archive_type && $archive_type != 'Wszystko' )
            $params['archive_type'] = $archive_type;

//        var_dump($params); die;


        $repository = $this->getDoctrine()->getRepository('CodersmillArchiveBundle:Archive');

//        if($archive_title == '')
//            $archives = $repository->findAllPublicArchivesBy(array('archive_type' => $archive_type));
//        else
//            $archives = $repository->findAllPublicArchivesBy(array('archive_type' => $archive_type, 'title' => $archive_title));

//        $archives = $repository->findAllPublicArchivesByTitle($archive_title);
        $archives = $repository->searchPublicArchives($params);
        $categories =  Archive::getArchiveModelNames();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $archives,
            $request->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );

        $archive_names = Archive::getArchiveModelNames();

        return $this->render('CodersmillArchiveBundle:Default:search.html.twig', array('archives' => $archives, 'pagination' => $pagination, 'categories' => $categories));
    }
}