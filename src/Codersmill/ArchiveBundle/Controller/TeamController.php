<?php

namespace Codersmill\ArchiveBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Codersmill\ArchiveBundle\Entity\Team;

/**
 * Team controller.
 *
 */
class TeamController extends BaseController
{

    /**
     * Lists all Team entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CodersmillArchiveBundle:Team')->findAll();

        return $this->render('CodersmillArchiveBundle:Team:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Team entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CodersmillArchiveBundle:Team')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Team entity.');
        }

        return $this->render('CodersmillArchiveBundle:Team:show.html.twig', array(
            'entity'      => $entity,
        ));
    }
}
