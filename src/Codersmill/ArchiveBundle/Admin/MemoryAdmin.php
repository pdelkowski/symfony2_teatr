<?php

namespace Codersmill\ArchiveBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class MemoryAdmin extends BaseAdmin
{
    const ARCHIVE_TYPE = 'Wspomnienia';

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        parent::configureFormFields($formMapper);

        $formMapper
            ->add('owner', 'text', array('label' => 'Nazwa właściciela obiektu'))
            ->add('archive_author', 'text', array('label' => 'Autor fotografii'))
            ->add('publish_house', 'text', array('label' => 'Wydawnictwo'))
            ->add('publish_date', 'date', array('label' => 'Data wydania'))
            ->add('publish_location', 'text', array('label' => 'Miejsce wydania'))
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        parent::configureListFields($listMapper);

        $listMapper
            ->add('owner')
            ->add('archive_author')
            ->add('publish_house')
        ;
    }
}
