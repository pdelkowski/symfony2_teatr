<?php

namespace Codersmill\ArchiveBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class LetterAdmin extends BaseAdmin
{
    const ARCHIVE_TYPE = 'List';

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        parent::configureFormFields($formMapper);

        $formMapper
            ->add('owner', 'text', array('label' => 'Nazwa właściciela obiektu'))
            ->add('archive_author', 'text', array('label' => 'Autor fotografii'))
            ->add('letter_from', 'text', array('label' => 'Nadawca listu'))
            ->add('letter_to', 'text', array('label' => 'Odbiorca listu'))
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        parent::configureListFields($listMapper);

        $listMapper
            ->add('letter_from')
            ->add('letter_to')
            ->add('archive_author')
        ;
    }
}
