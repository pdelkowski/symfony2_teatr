<?php

namespace Codersmill\ArchiveBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class BaseAdmin extends Admin
{
    const ARCHIVE_TYPE = 'Archiwum';

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('scan_number', 'text', array('label' => 'Numer skanu'))
            ->add('title', 'text', array('label' => 'Tytuł'))
            ->add('slug', 'text', array('label' => 'Url slug (SEO)'))
            ->add('description', 'textarea', array('label' => 'Opis'))
            ->add('format', 'text', array('label' => 'Format'))
            ->add('digital_creator', 'text', array('label' => 'Imię i nazwisko osoby wykonującej kopię cyfrową'))
            ->add('signature', 'text', array('label' => 'Sygnatura'))
            ->add('domain', 'text', array('label' => 'Prawa'))
            ->add('date_created', 'text', array('label' => 'Data napisania listu'))
            //->add('date_created', 'date', array('label' => 'Data napisania listu'))
            ->add('date_created_digital', 'text', array('label' => 'Data utworzenia kopi cyfrowej'))
            //->add('date_created_digital', 'date', array('label' => 'Data utworzenia kopi cyfrowej'))
            ->add('storage_location', 'text', array('label' => 'Miejsce przechowywania oryginału'))
            ->add('device', 'text', array('label' => 'Typ urządzenia'))
            ->add('author', 'entity', array('label' => 'Author wpisu', 'class' => 'Application\Sonata\UserBundle\Entity\User'))
            ->add('access', 'choice', array('label' => 'Widoczność', 'choices' => array(
                'public' => 'Publiczny',
                'request' => 'Na żądanie',
                'privated' => 'Prywatny' 
            )))
//            ->add('tags_name', 'entity', array('label' => 'Tagi', 'class' => 'Codersmill\ArchiveBundle\Entity\Tag'))
            ->add('archive_type', 'hidden', array('data' => static::ARCHIVE_TYPE))
            ->end()
            ->with('Tagi', array('collapsed' => true))
                ->add('tags', 'entity', array('label' => 'Tagi', 'expanded' => true, 'by_reference' => false, 'multiple' => true, 'class' => 'Codersmill\ArchiveBundle\Entity\Tag'))
            ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('scan_number')
            ->add('title')
            ->add('digital_creator')
            ->add('date_created')
            ->add('access')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('scan_number')
            ->add('title')
            ->add('digital_creator')
            ->add('date_created')
            ->add('signature')
            ->add('storage_location')
            ->add('access')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
//        $collection->clear();
    }
}
