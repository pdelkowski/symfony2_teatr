<?php

namespace Codersmill\ArchiveBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class ArchiveAdmin extends Admin
{
    const ARCHIVE_TYPE = 'Archiwum';

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', 'text', array('label' => 'Archive Title'))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('scan_number')
            ->add('archive_type')
            ->add('digital_creator')
            ->add('storage_location')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('scan_number')
            ->add('title')
            ->add('digital_creator')
            ->add('date_created')
            ->add('storage_location')
            ->add('thumb')
            ->add('archive_type')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
//        $collection->clear();
//        $collection->add('list');
//        $collection->add('create');
//        $collection->add('delete');
//        $collection->add('export');
        $collection->remove('create');
        $collection->add('import');
        $collection->add('importProcess');
        $collection->add('importSave');
    }

    public function getBatchActions()
    {
        // retrieve the default batch actions (currently only delete)
        $actions = parent::getBatchActions();

        if (
            $this->hasRoute('delete') && $this->isGranted('DELETE')
        ) {
            $actions['generate'] = array(
                'label' => 'Generuj obrazki',
                'ask_confirmation' => false
            );

            $actions['generateAll'] = array(
                'label' => 'Generuj miniatury dla wszystkich',
                'ask_confirmation' => false
            );

        }

        return $actions;
    }


}
