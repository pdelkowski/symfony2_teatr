<?php

namespace Codersmill\ArchiveBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class TeamAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('firstname', 'text', array('label' => 'Imię'))
            ->add('lastname', 'text', array('label' => 'Nazwisko'))
            ->add('speciality', 'text', array('label' => 'Specializacja'))
            ->add('description', 'textarea', array('label' => 'Opis'))
            ->add('file', 'file', array('label' => 'Zdjęcie', 'data_class' => NULL))
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('lastname')
            ->add('firstname')
            ->add('speciality')
            ->add('date_created')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
//        $collection->clear();
//        $collection->add('list');
//        $collection->add('create');
//        $collection->add('export');
//        $collection->add('import');
//        $collection->add('importProcess');
    }

    public function prePersist($image) {
        $this->manageFileUpload($image);
    }

    public function preUpdate($image) {
        $this->manageFileUpload($image);
    }

    private function manageFileUpload($image) {
        if ($image->getFile()) {
            $image->refreshUpdated();
        }
    }


}
