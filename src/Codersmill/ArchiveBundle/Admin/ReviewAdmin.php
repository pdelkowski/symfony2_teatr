<?php

namespace Codersmill\ArchiveBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ReviewAdmin extends BaseAdmin
{
    const ARCHIVE_TYPE = 'Recenzja';

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        parent::configureFormFields($formMapper);

        $formMapper
            ->add('owner', 'text', array('label' => 'Nazwa właściciela obiektu'))
            ->add('play', 'text', array('label' => 'Tytuł sztuki'))
            ->add('art_play', 'text', array('label' => 'Tytuł realizacji'))
            ->add('theater', 'text', array('label' => 'Nazwa teatru'))
            ->add('archive_author', 'text', array('label' => 'Autor fotografii'))
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        parent::configureListFields($listMapper);

        $listMapper
            ->add('play')
            ->add('art_play')
            ->add('theatr')
            ->add('archive_author')
        ;
    }

}
