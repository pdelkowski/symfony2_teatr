<?php

namespace Codersmill\ArchiveBundle\Entity;

class Letter extends Archive 
{
	protected $letter_from;

	protected $letter_to;

	protected $owner;

    protected $archive_author;

    /**
     * Set letter_from
     *
     * @param string $letterFrom
     * @return Letter
     */
    public function setLetterFrom($letterFrom)
    {
        $this->letter_from = $letterFrom;

        return $this;
    }

    /**
     * Get letter_from
     *
     * @return string 
     */
    public function getLetterFrom()
    {
        return $this->letter_from;
    }

    /**
     * Set letter_to
     *
     * @param string $letterTo
     * @return Letter
     */
    public function setLetterTo($letterTo)
    {
        $this->letter_to = $letterTo;

        return $this;
    }

    /**
     * Get letter_to
     *
     * @return string 
     */
    public function getLetterTo()
    {
        return $this->letter_to;
    }

    /**
     * Set owner
     *
     * @param string $owner
     * @return Letter
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return string 
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set archive_author
     *
     * @param string $archiveAuthor
     * @return Memory
     */
    public function setArchiveAuthor($archiveAuthor)
    {
        $this->archive_author = $archiveAuthor;

        return $this;
    }

    /**
     * Get archive_author
     *
     * @return string
     */
    public function getArchiveAuthor()
    {
        return $this->archive_author;
    }

    public static function getArchiveModelFieldNames($field_map=true)
    {
        $fields = parent::getArchiveModelFieldNames($field_map);

        if($field_map)
            $extra_fields = array(
                'letter_from'       => 'Adresat listu',
                'letter_to'         => 'Odbiorca listu',
                'owner'             => 'Właściciel',
                'archive_author'    => 'Twórca'
            );
        else
            $extra_fields = array('letter_from', 'letter_to', 'owner', 'archive_author');

        return array_merge($extra_fields, $fields);
    }
}
