<?php

namespace Codersmill\ArchiveBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class Category 
{
	protected $id;

	protected $title;

	protected $slug;

	protected $description;

	protected $archives;

	protected $property;

	public function __construct()
	{
		$this->archives = new ArrayCollection();
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function setTitle($title)
	{
		$this->title = $title;
	}

	public function getSlug()
	{
		return $this->slug;
	}

	public function setSlug($slug)
	{
		$this->slug = $slug;
	}

	public function getDescription()
	{
		return $this->description;
	}

	public function setDescription($desc)
	{
		$this->description = $desc;
	}

	public function getArchives()
	{
		return $this->archives;
	}

	public function setArchives($archives)
	{
		$this->archives = $archives;
	}

	public function __toString()
	{
		return $this->getTitle();
	}
}
