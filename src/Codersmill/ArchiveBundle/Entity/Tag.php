<?php

namespace Codersmill\ArchiveBundle\Entity;

class Tag
{
	protected $id;

	protected $tag_name;

    protected $tag_desc;

    private $archives;

    public function __construct() {
        $this->archives = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getArchives()
    {
        return $this->archives;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Archive
     */
    public function setTagName($tag_name)
    {
        $this->tag_name = $tag_name;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTagName()
    {
        return $this->tag_name;
    }


    public function setTagDesc($tag_desc)
    {
        $this->tag_desc = $tag_desc;

        return $this;
    }

    public function getTagDesc()
    {
        return $this->tag_desc;
    }

    public function __toString()
    {
        return $this->tag_name;
    }

}
