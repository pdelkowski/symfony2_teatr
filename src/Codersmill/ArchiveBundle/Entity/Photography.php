<?php

namespace Codersmill\ArchiveBundle\Entity;

class Photography extends Archive
{
	protected $archive_author;

    /**
     * Set archive_author
     *
     * @param string $archiveAuthor
     * @return Photography
     */
    public function setArchiveAuthor($archiveAuthor)
    {
        $this->archive_author = $archiveAuthor;

        return $this;
    }

    /**
     * Get archive_author
     *
     * @return string 
     */
    public function getArchiveAuthor()
    {
        return $this->archive_author;
    }


    public static function getArchiveModelFieldNames($field_map=true)
    {
        $fields = parent::getArchiveModelFieldNames($field_map);

        if($field_map)
            $extra_fields = array(
                'archive_author'    => 'Twórca'
            );
        else
            $extra_fields = array('archive_author');

        return array_merge($extra_fields, $fields);
    }
}
