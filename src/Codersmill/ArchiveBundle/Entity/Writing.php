<?php

namespace Codersmill\ArchiveBundle\Entity;

class Writing extends Archive
{
	protected $subtitle;

	protected $owner;

	protected $archive_author;

	protected $publish_house;

	protected $publish_location;

	protected $publish_date;

    /**
     * Set subtitle
     *
     * @param string $subtitle
     * @return Publication
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Get subtitle
     *
     * @return string 
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Set owner
     *
     * @param string $owner
     * @return Publication
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return string 
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set archive_author
     *
     * @param string $archiveAuthor
     * @return Publication
     */
    public function setArchiveAuthor($archiveAuthor)
    {
        $this->archive_author = $archiveAuthor;

        return $this;
    }

    /**
     * Get archive_author
     *
     * @return string 
     */
    public function getArchiveAuthor()
    {
        return $this->archive_author;
    }

    /**
     * Set publish_house
     *
     * @param string $publishHouse
     * @return Publication
     */
    public function setPublishHouse($publishHouse)
    {
        $this->publish_house = $publishHouse;

        return $this;
    }

    /**
     * Get publish_house
     *
     * @return string 
     */
    public function getPublishHouse()
    {
        return $this->publish_house;
    }

    /**
     * Set publish_location
     *
     * @param string $publishLocation
     * @return Publication
     */
    public function setPublishLocation($publishLocation)
    {
        $this->publish_location = $publishLocation;

        return $this;
    }

    /**
     * Get publish_location
     *
     * @return string 
     */
    public function getPublishLocation()
    {
        return $this->publish_location;
    }

    /**
     * Set publish_date
     *
     * @param \DateTime $publishDate
     * @return Publication
     */
    public function setPublishDate($publishDate)
    {
        $this->publish_date = $publishDate;

        return $this;
    }

    /**
     * Get publish_date
     *
     * @return \DateTime 
     */
    public function getPublishDate()
    {
        return $this->publish_date;
    }



    public static function getArchiveModelFieldNames($field_map=true)
    {
        $fields = parent::getArchiveModelFieldNames($field_map);

        if($field_map)
            $extra_fields = array(
                'subtitle'          => 'Podtytuł',
                'owner'             => 'Właściciel',
                'archive_author'    => 'Twórca',
                'publish_house'     => 'Wydawnictwo',
                'publish_date'      => 'Data publikacji',
                'publish_location'  => 'Miejsce publikacji'
            );
        else
            $extra_fields = array('subtitle', 'owner', 'archive_author', 'publish_house', 'publish_date', 'publish_location');

        return array_merge($extra_fields, $fields);
    }
}
