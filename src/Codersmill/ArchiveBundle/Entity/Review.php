<?php

namespace Codersmill\ArchiveBundle\Entity;

class Review extends Archive
{
	protected $owner;

    protected $play;

    protected $art_play;

    protected $theater;

	protected $archive_author;

    /**
     * Set owner
     *
     * @param string $owner
     * @return Document
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return string
     */
    public function getOwner()
    {
        return $this->owner;
    }


    public function getPlay()
    {
        return $this->play;
    }

    public function setPlay($play)
    {
        $this->play = $play;

        return $this;
    }

    public function getArtPlay()
    {
        return $this->art_play;
    }

    public function setArtPlay($play)
    {
        $this->art_play = $play;

        return $this;
    }

    public function getTheater()
    {
        return $this->theater;
    }

    public function setTheater($theater)
    {
        $this->theater = $theater;

        return $this;
    }



    /**
     * Set archive_author
     *
     * @param string $archiveAuthor
     * @return Document
     */
    public function setArchiveAuthor($archiveAuthor)
    {
        $this->archive_author = $archiveAuthor;

        return $this;
    }

    /**
     * Get archive_author
     *
     * @return string 
     */
    public function getArchiveAuthor()
    {
        return $this->archive_author;
    }



    public static function getArchiveModelFieldNames($field_map=true)
    {
        $fields = parent::getArchiveModelFieldNames($field_map);

        if($field_map)
            $extra_fields = array(
                'owner' => 'Właściciel',
                'archive_author' => 'Twórca',
                'description' => 'Zródło',
                'title' => 'Tytuł artykułu',
                'theater'   => 'Nazwa teatru',
                'play'  => 'Tytuł sztuki',
                'art_play' => 'Tytuł realizacji'
            );
        else
            $extra_fields = array('owner', 'archive_author', 'theater', 'play', 'art_play');

        return array_merge($fields, $extra_fields);
    }
}
