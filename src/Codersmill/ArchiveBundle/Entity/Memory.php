<?php

namespace Codersmill\ArchiveBundle\Entity;

class Memory extends Archive 
{
	protected $owner;

	protected $archive_author;

    protected $publish_house;

	protected $publish_date;

	protected $publish_location;

    /**
     * Set owner
     *
     * @param string $owner
     * @return Memory
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return string 
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set archive_author
     *
     * @param string $archiveAuthor
     * @return Memory
     */
    public function setArchiveAuthor($archiveAuthor)
    {
        $this->archive_author = $archiveAuthor;

        return $this;
    }

    /**
     * Get archive_author
     *
     * @return string 
     */
    public function getArchiveAuthor()
    {
        return $this->archive_author;
    }


    /**
     * Set publish_location
     *
     * @param string $publishLocation
     * @return Memory
     */
    public function setPublishLocation($publishLocation)
    {
        $this->publish_location = $publishLocation;

        return $this;
    }

    /**
     * Get publish_location
     *
     * @return string 
     */
    public function getPublishLocation()
    {
        return $this->publish_location;
    }

    /**
     * Set publish_date
     *
     * @param \DateTime $publishDate
     * @return Memory
     */
    public function setPublishDate($publishDate)
    {
        $this->publish_date = $publishDate;

        return $this;
    }

    /**
     * Get publish_date
     *
     * @return \DateTime 
     */
    public function getPublishDate()
    {
        return $this->publish_date;
    }

    /**
     * Set publish_house
     *
     * @param string $publishHouse
     * @return Memory
     */
    public function setPublishHouse($publishHouse)
    {
        $this->publish_house = $publishHouse;

        return $this;
    }

    /**
     * Get publish_house
     *
     * @return string 
     */
    public function getPublishHouse()
    {
        return $this->publish_house;
    }


    public static function getArchiveModelFieldNames($field_map=true)
    {
        $fields = parent::getArchiveModelFieldNames($field_map);

        if($field_map)
            $extra_fields = array(
                'owner'             => 'Właściciel',
                'archive_author'    => 'Twórca',
                'publish_house'     => 'Wydawnictwo',
                'publish_date'      => 'Data publikacji',
                'publish_location'  => 'Miejsce publikacji'
            );
        else
            $extra_fields = array('owner', 'archive_author', 'publish_house', 'publish_date', 'publish_location');

        return array_merge($extra_fields, $fields);
    }
}
