<?php

namespace Codersmill\ArchiveBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Codersmill\ArchiveBundle as ArchiveBundle;

class ArchiveRepository extends EntityRepository
{
    public function getAllPublicArchives()
    {
        return $this->getEntityManager()
            ->createQuery(
                "SELECT p FROM CodersmillArchiveBundle:Archive p WHERE p.scan_number NOT LIKE '%p.TIFF' ORDER BY p.description DESC"
            )
            ->getResult();
    }

    public function findAllPublicArchivesBy(array $options)
    {
        $archive_type = $options['archive_type'];
        $title = isset($options['title']) ? $options['title'] : NULL;

        if($title) {
            return $this->getEntityManager()
                ->createQuery(
                    "SELECT p FROM CodersmillArchiveBundle:Archive p WHERE p.scan_number NOT LIKE '%p.TIFF' AND p.archive_type = '".$archive_type."' AND p.title = '".$title."'"
                )
                ->getResult();
        }

        return $this->getEntityManager()
            ->createQuery(
                "SELECT p FROM CodersmillArchiveBundle:Archive p WHERE p.scan_number NOT LIKE '%p.TIFF' AND p.archive_type = '".$archive_type."'"
            )
                ->getResult();
    }

    public function findAllPublicArchivesByTitle($title)
    {
        if($title) {
            return $this->getEntityManager()
                ->createQuery(
                    "SELECT p FROM CodersmillArchiveBundle:Archive p WHERE p.scan_number NOT LIKE '%p.TIFF' AND p.title = '".$title."'"
                )
                ->getResult();
        }

        return NULL;
    }

    public function searchPublicArchives($params)
    {
        $criteria_params = array();
        $em = $this->getEntityManager();
        $q = $em->createQueryBuilder()
                ->select('a')
                ->from('CodersmillArchiveBundle:Archive', 'a');

        // All archive with the ending "p.TIFF" are NOT public so exclude them from query
        $q->where($q->expr()->notLike('a.scan_number', ':private_archive'));
        $criteria_params['private_archive'] = '%p.TIFF';

//        echo '<pre>';
//        var_dump($params); die;

        if( isset($params['title']) and $params['title'] != '' )
        {
            $q->andWhere('a.title = :title');
            $criteria_params['title'] = $params['title'];
        }

        if( isset($params['date']) and $params['date'] != '' )
        {
            $q->andWhere('a.date_created = :date');
            $criteria_params['date'] = $params['date'];
        }

        if( isset($params['author']) and $params['author'] != '' )
        {
            $q->andWhere('a.author = :author');
            $criteria_params['author'] = $params['author'];
        }

        if( isset($params['archive_type']) and $params['archive_type'] != '' )
        {
            $q->andWhere('a.archive_type = :archive_type');
            $criteria_params['archive_type'] = $params['archive_type'];
        }

        $q->setParameters($criteria_params);

        return $q->getQuery()->getResult();
    }

    public function getLatestArchives()
    {
        $em = $this->getEntityManager();
        $q = $em->createQueryBuilder()
                ->add('select', 'a')
                ->add('from', 'CodersmillArchiveBundle:Archive a')
                ->add('orderBy', 'a.date_created DESC')
                ->setFirstResult(0)
                ->setMaxResults(5)
                ->getQuery();

        return $q->getResult();
    }
}