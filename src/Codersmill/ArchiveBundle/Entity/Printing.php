<?php

namespace Codersmill\ArchiveBundle\Entity;

class Printing extends Archive 
{
	protected $owner;

    /**
     * Set owner
     *
     * @param string $owner
     * @return Printing
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return string 
     */
    public function getOwner()
    {
        return $this->owner;
    }


    public static function getArchiveModelFieldNames($field_map=true)
    {
        $fields = parent::getArchiveModelFieldNames($field_map);

        if($field_map)
            $extra_fields = array(
                'owner'             => 'Właściciel',
            );
        else
            $extra_fields = array('owner');

        return array_merge($extra_fields, $fields);
    }
}
