<?php

namespace Codersmill\ArchiveBundle\Entity;

class Drawing extends Archive 
{
	protected $owner;

	protected $archive_author;

    /**
     * Set owner
     *
     * @param string $owner
     * @return Drawing
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return string 
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set archive_author
     *
     * @param string $archiveAuthor
     * @return Drawing
     */
    public function setArchiveAuthor($archiveAuthor)
    {
        $this->archive_author = $archiveAuthor;

        return $this;
    }

    /**
     * Get archive_author
     *
     * @return string 
     */
    public function getArchiveAuthor()
    {
        return $this->archive_author;
    }


    public static function getArchiveModelFieldNames($field_map=true)
    {
        $fields = parent::getArchiveModelFieldNames($field_map);

        if($field_map)
            $extra_fields = array(
                'owner'             => 'Właściciel',
                'archive_author'    => 'Twórca'
            );
        else
            $extra_fields = array('owner', 'archive_author');

        return array_merge($extra_fields, $fields);
    }
}
