<?php

namespace Codersmill\ArchiveBundle\Entity;

class Archive 
{
	protected $id;

	protected $scan_number;

	protected $title;

	protected $slug;

	protected $description;

	protected $format;

	protected $digital_creator;

	protected $signature;

	protected $domain;

	protected $access;	

	protected $date_created;

	protected $date_created_digital;

	protected $storage_location;

	protected $device;

	protected $author;

	protected $archive_type;

    protected $thumb;

    public $tags;

    public function __construct() {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function addTags($tags)
    {
        $this->tags[] = $tags;
//        $partner->setDistrict($this); // This line is important for Sonata.
    }

    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set scan_number
     *
     * @param string $scanNumber
     * @return Archive
     */
    public function setScanNumber($scanNumber)
    {
        $this->scan_number = $scanNumber;

        return $this;
    }

    /**
     * Get scan_number
     *
     * @return string 
     */
    public function getScanNumber()
    {
        return $this->scan_number;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Archive
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Archive
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Archive
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set format
     *
     * @param string $format
     * @return Archive
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get format
     *
     * @return string 
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set digital_creator
     *
     * @param string $digitalCreator
     * @return Archive
     */
    public function setDigitalCreator($digitalCreator)
    {
        $this->digital_creator = $digitalCreator;

        return $this;
    }

    /**
     * Get digital_creator
     *
     * @return string 
     */
    public function getDigitalCreator()
    {
        return $this->digital_creator;
    }

    /**
     * Set signature
     *
     * @param string $signature
     * @return Archive
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * Get signature
     *
     * @return string 
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * @return Archive
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string 
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set access
     *
     * @param string $access
     * @return Archive
     */
    public function setAccess($access)
    {
        $this->access = $access;

        return $this;
    }

    /**
     * Get access
     *
     * @return string 
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * Set date_created
     *
     * @param \DateTime $dateCreated
     * @return Archive
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get date_created
     *
     * @return \DateTime 
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set date_created_digital
     *
     * @param \DateTime $dateCreatedDigital
     * @return Archive
     */
    public function setDateCreatedDigital($dateCreatedDigital)
    {
        $this->date_created_digital = $dateCreatedDigital;

        return $this;
    }

    /**
     * Get date_created_digital
     *
     * @return \DateTime 
     */
    public function getDateCreatedDigital()
    {
        return $this->date_created_digital;
    }

    /**
     * Set storage_location
     *
     * @param string $storageLocation
     * @return Archive
     */
    public function setStorageLocation($storageLocation)
    {
        $this->storage_location = $storageLocation;

        return $this;
    }

    /**
     * Get storage_location
     *
     * @return string 
     */
    public function getStorageLocation()
    {
        return $this->storage_location;
    }

    /**
     * Set device
     *
     * @param string $device
     * @return Archive
     */
    public function setDevice($device)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device
     *
     * @return string 
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * Set archive_type
     *
     * @param string $archiveType
     * @return Archive
     */
    public function setArchiveType($archiveType)
    {
        $this->archive_type = $archiveType;

        return $this;
    }

    /**
     * Get archive_type
     *
     * @return string 
     */
    public function getArchiveType()
    {
        return $this->archive_type;
    }

    /**
     * Set thumb
     *
     * @param string thumb
     * @return Archive
     */
    public function setThumb($thumb)
    {
        $this->thumb = $thumb;

        return $this;
    }

    /**
     * Get thumb
     *
     * @return string
     */
    public function getThumb()
    {
        return $this->thumb;
    }

    /**
     * Set author
     *
     * @param \Application\Sonata\UserBundle\Entity\User $author
     * @return Archive
     */
    public function setAuthor(\Application\Sonata\UserBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getAuthor()
    {
        return $this->author;
    }


    /**
     * Get all available archive model names
     *
     * @return array
     */
    public static function getArchiveModelNames($class_map=true, $combined = true)
    {
        $models = array();

        if($class_map)
        {
            $models = array(
                'Document'      => 'Dokument',
                'Drawing'       => 'Rysunek',
                'Image'         => 'Obraz',
                'Letter'        => 'List',
                'Memory'        => 'Wspomnienia',
                'Object3D'      => 'Obiekt 3D',
                'Photography'   => 'Fotografia',
                'Poetry'        => 'Poezja',
                'Printing'      => 'Druk',
                'Publication'   => 'Publikacja',
                'Manuscript'    => 'Rękopis',
                'Writing'       => 'Dzieło',
                'Review'       => 'Recenzja'
            );

        } else {
            $models = array('Dokument', 'Rysunek', 'Obraz', 'List', 'Wspomnienia', 'Obiekt 3D', 'Fotografia', 'Poezja', 'Druk', 'Publikacja', 'Rękopis', 'Dzieło', 'Recenzja');

            // set the keys same as values
            if($combined)
                $models = array_combine($models, $models);
        }

        return $models;
    }


    public static function getArchiveModelFieldNames($field_map=true)
    {
        $fields = array();

        if($field_map)
        {
            $fields = array(
                'scan_number' => 'Numer skanu',
                'title' => 'Tytuł',
                'description' => 'Opis',
                'format' => 'Format',
                'digital_creator' => 'Twórca kopi cyfrowej',
                'signature' => 'Sygnatura',
                'domain' => 'Prawa',
                'date_created' => 'Data utworzenia',
                'date_created_digital' => 'Data utworzenia kopi cyforowej',
                'storage_location' => 'Miejsce przechowania',
                'device' => 'Urządzenie',
            );

        } else {
            $fields = array('scan_number', 'title', 'description', 'format', 'digital_creator', 'signature',
                        'domain', 'date_created', 'date_created_digital', 'storage_location', 'device', 'author');
        }

        return $fields;
    }


    public static function getArchiveFieldsByModelName($model_name)
    {
        $class = mb_convert_case($model_name, MB_CASE_TITLE, "UTF-8");
        $model = '\\Codersmill\\ArchiveBundle\\Entity\\'.$model_name;
        return $model::getArchiveModelFieldNames();
    }

}
