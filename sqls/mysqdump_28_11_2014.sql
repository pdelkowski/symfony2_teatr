-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: teatr
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acl_classes`
--

DROP TABLE IF EXISTS `acl_classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_classes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_69DD750638A36066` (`class_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_classes`
--

LOCK TABLES `acl_classes` WRITE;
/*!40000 ALTER TABLE `acl_classes` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_entries`
--

DROP TABLE IF EXISTS `acl_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_entries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int(10) unsigned NOT NULL,
  `object_identity_id` int(10) unsigned DEFAULT NULL,
  `security_identity_id` int(10) unsigned NOT NULL,
  `field_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ace_order` smallint(5) unsigned NOT NULL,
  `mask` int(11) NOT NULL,
  `granting` tinyint(1) NOT NULL,
  `granting_strategy` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `audit_success` tinyint(1) NOT NULL,
  `audit_failure` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4` (`class_id`,`object_identity_id`,`field_name`,`ace_order`),
  KEY `IDX_46C8B806EA000B103D9AB4A6DF9183C9` (`class_id`,`object_identity_id`,`security_identity_id`),
  KEY `IDX_46C8B806EA000B10` (`class_id`),
  KEY `IDX_46C8B8063D9AB4A6` (`object_identity_id`),
  KEY `IDX_46C8B806DF9183C9` (`security_identity_id`),
  CONSTRAINT `FK_46C8B8063D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_46C8B806DF9183C9` FOREIGN KEY (`security_identity_id`) REFERENCES `acl_security_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_46C8B806EA000B10` FOREIGN KEY (`class_id`) REFERENCES `acl_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_entries`
--

LOCK TABLES `acl_entries` WRITE;
/*!40000 ALTER TABLE `acl_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_object_identities`
--

DROP TABLE IF EXISTS `acl_object_identities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_object_identities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_object_identity_id` int(10) unsigned DEFAULT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `object_identifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entries_inheriting` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9407E5494B12AD6EA000B10` (`object_identifier`,`class_id`),
  KEY `IDX_9407E54977FA751A` (`parent_object_identity_id`),
  CONSTRAINT `FK_9407E54977FA751A` FOREIGN KEY (`parent_object_identity_id`) REFERENCES `acl_object_identities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_object_identities`
--

LOCK TABLES `acl_object_identities` WRITE;
/*!40000 ALTER TABLE `acl_object_identities` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_object_identities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_object_identity_ancestors`
--

DROP TABLE IF EXISTS `acl_object_identity_ancestors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_object_identity_ancestors` (
  `object_identity_id` int(10) unsigned NOT NULL,
  `ancestor_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`object_identity_id`,`ancestor_id`),
  KEY `IDX_825DE2993D9AB4A6` (`object_identity_id`),
  KEY `IDX_825DE299C671CEA1` (`ancestor_id`),
  CONSTRAINT `FK_825DE2993D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_825DE299C671CEA1` FOREIGN KEY (`ancestor_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_object_identity_ancestors`
--

LOCK TABLES `acl_object_identity_ancestors` WRITE;
/*!40000 ALTER TABLE `acl_object_identity_ancestors` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_object_identity_ancestors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_security_identities`
--

DROP TABLE IF EXISTS `acl_security_identities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_security_identities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8835EE78772E836AF85E0677` (`identifier`,`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_security_identities`
--

LOCK TABLES `acl_security_identities` WRITE;
/*!40000 ALTER TABLE `acl_security_identities` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_security_identities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive`
--

DROP TABLE IF EXISTS `archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `scan_number` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `format` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `digital_creator` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `signature` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `access` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_created_digital` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `storage_location` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `device` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `archive_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `letter_from` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `letter_to` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `owner` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `archive_disc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `archive_author` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `publish_house` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `publish_location` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `publish_date` date DEFAULT NULL,
  `subtitle` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D5FC5D9CF675F31B` (`author_id`),
  CONSTRAINT `FK_D5FC5D9CF675F31B` FOREIGN KEY (`author_id`) REFERENCES `fos_user_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive`
--

LOCK TABLES `archive` WRITE;
/*!40000 ALTER TABLE `archive` DISABLE KEYS */;
INSERT INTO `archive` VALUES (56,NULL,'Zasłona Mohameta','url','rękopis, wiersz przepisany przez prawnuczkę Aleksandra Fredry Jadwigę Szeptycką','ZP_IZP_P_K1_1_0_001p.TIFF','600 dpi TIFF, 6302x7817','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-31','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Aleksander Fredro',NULL,NULL,NULL,NULL,'ZP_IZP_P_K1_1_0_001p.TIFF.jpg'),(57,NULL,'Zasłona Mohameta','url','rękopis, wiersz przepisany przez prawnuczkę Aleksandra Fredry Jadwigę Szeptycką','ZP_IZP_P_K1_1_0_001.TIFF','600 dpi TIFF, 4790x7779','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-31','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Aleksander Fredro',NULL,NULL,NULL,NULL,'ZP_IZP_P_K1_1_0_001.TIFF.jpg'),(58,NULL,'Stary śpiewak','url','rękopis, wiersz przepisany przez prawnuczkę Aleksandra Fredry Jadwigę Szeptycką','ZP_IZP_P_K1_2_0_001p.TIFF','600 dpi TIFF, 6274x7663','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-31','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Aleksander Fredro',NULL,NULL,NULL,NULL,'ZP_IZP_P_K1_2_0_001p.TIFF.jpg'),(59,NULL,'Stary śpiewak','url','rękopis, wiersz przepisany przez prawnuczkę Aleksandra Fredry Jadwigę Szeptycką','ZP_IZP_P_K1_2_0_001.TIFF','600 dpi TIFF, 4790x7691','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-31','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Aleksander Fredro',NULL,NULL,NULL,NULL,'ZP_IZP_P_K1_2_0_001.TIFF.jpg'),(60,NULL,'Stary śpiewak','url','rękopis, wiersz przepisany przez prawnuczkę Aleksandra Fredry Jadwigę Szeptycką','ZP_IZP_P_K1_2_0_002.TIFF','600 dpi TIFF, 4857x7821','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-31','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Aleksander Fredro',NULL,NULL,NULL,NULL,'ZP_IZP_P_K1_2_0_002.TIFF.jpg'),(61,NULL,'Mój pacież','url','rękopis, wiersz przepisany przez wnuczkę Aleksandra Fredry Marię z Fredrów Szembekową (zwaną Mimi)','ZP_IZP_P_K1_3_0_001p.TIFF','600 dpi TIFF, 6386x7779','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-31','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Aleksander Fredro',NULL,NULL,NULL,NULL,'ZP_IZP_P_K1_3_0_001p.TIFF.jpg'),(62,NULL,'Mój pacież','url','rękopis, wiersz przepisany przez wnuczkę Aleksandra Fredry Marię z Fredrów Szembekową (zwaną Mimi)','ZP_IZP_P_K1_3_0_001.TIFF','600 dpi TIFF, 4818x7749','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-31','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Aleksander Fredro',NULL,NULL,NULL,NULL,'ZP_IZP_P_K1_3_0_001.TIFF.jpg'),(63,NULL,'Mój pacież','url','rękopis, wiersz przepisany przez wnuczkę Aleksandra Fredry Marię z Fredrów Szembekową (zwaną Mimi)','ZP_IZP_P_K1_3_0_002.TIFF','600 dpi TIFF, 4876x7749','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-31','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Aleksander Fredro',NULL,NULL,NULL,NULL,'ZP_IZP_P_K1_3_0_002.TIFF.jpg'),(64,NULL,'Mój pacież','url','rękopis, wiersz przepisany przez wnuczkę Aleksandra Fredry Marię z Fredrów Szembekową (zwaną Mimi)','ZP_IZP_P_K1_3_0_003.TIFF','600 dpi TIFF, 4827x7792','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-31','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Aleksander Fredro',NULL,NULL,NULL,NULL,'ZP_IZP_P_K1_3_0_003.TIFF.jpg'),(65,NULL,'Mój pacież','url','rękopis, wiersz przepisany przez wnuczkę Aleksandra Fredry Marię z Fredrów Szembekową (zwaną Mimi)','ZP_IZP_P_K1_3_0_004.TIFF','600 dpi TIFF, 4857x7792','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-31','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Aleksander Fredro',NULL,NULL,NULL,NULL,'ZP_IZP_P_K1_3_0_004.TIFF.jpg'),(66,NULL,'Krzysi – do pamiętnika','url','maszynopis','ZP_IZP_P_K2_7_0_001p.TIFF','600 dpi TIFF, 4785x3521','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(67,NULL,'Krzysi – do pamiętnika','url','maszynopis','ZP_IZP_P_K2_7_0_001.TIFF','600 dpi TIFF, 4785x3427','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(68,NULL,'Ideał równości','url','rękopis','ZP_IZP_P_K2_1_0_001p.TIFF','600 dpi TIFF, 4125x4793','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(69,NULL,'Ideał równości','url','rękopis','ZP_IZP_P_K2_1_0_001.TIFF','600 dpi TIFF, 2675x4193','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(70,NULL,'Ideał równości','url','rękopis','ZP_IZP_P_K2_1_0_002.TIFF','600 dpi TIFF, 2675x4193','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(71,NULL,'Ideał równości','url','rękopis','ZP_IZP_P_K2_1_0_003.TIFF','600 dpi TIFF, 2675x4193','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(72,NULL,'Nie wódź nas na pokuszenie...','url','rękopis','ZP_IZP_P_K2_2_0_001p.TIFF','600 dpi TIFF, 4796x5253','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(73,NULL,'Nie wódź nas na pokuszenie...','url','rękopis','ZP_IZP_P_K2_2_0_001.TIFF','600 dpi TIFF, 3359x5253','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(74,NULL,'Nie wódź nas na pokuszenie...','url','rękopis','ZP_IZP_P_K2_2_0_002.TIFF','600 dpi TIFF, 3359x5253','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(75,NULL,'Nie wódź nas na pokuszenie...','url','rękopis','ZP_IZP_P_K2_2_0_003.TIFF','600 dpi TIFF, 3359x5253','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(76,NULL,'Litewskie bory','url','rękopis','ZP_IZP_P_K2_3_0_001p.TIFF','600 dpi TIFF, 4019x4758','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(77,NULL,'Litewskie bory','url','rękopis','ZP_IZP_P_K2_3_0_001.TIFF','600 dpi TIFF, 2569x4075','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(78,NULL,'Litewskie bory','url','rękopis','ZP_IZP_P_K2_3_0_002.TIFF','600 dpi TIFF, 2569x4075','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(79,NULL,'Rozmowa','url','maszynopis, wiersz ukazał się w “Wierszach ulotnych”, wyd. II, 1893','ZP_IZP_P_K2_4_0_001p.TIFF','600 dpi TIFF, 4950x7008','Monika Żytkowiak','brak','własność prywatna','public','Około 1893','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(80,NULL,'Rozmowa','url','maszynopis, wiersz ukazał się w “Wierszach ulotnych”, wyd. II, 1893','ZP_IZP_P_K2_4_0_001.TIFF','600 dpi TIFF, 4950x7008','Monika Żytkowiak','brak','własność prywatna','public','Około 1893','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(81,NULL,'Rozmowa','url','maszynopis, wiersz ukazał się w “Wierszach ulotnych”, wyd. II, 1893','ZP_IZP_P_K2_4_0_002.TIFF','600 dpi TIFF, 4950x7008','Monika Żytkowiak','brak','własność prywatna','public','Około 1893','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(82,NULL,'Ballada jakich mało','url','maszynopis, na końcu z odręczną adnotacją','ZP_IZP_P_K2_5_0_001p.TIFF','600 dpi TIFF, 4950x7008','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(83,NULL,'Ballada jakich mało','url','maszynopis, na końcu z odręczną adnotacją','ZP_IZP_P_K2_5_0_001.TIFF','600 dpi TIFF, 4950x7008','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(84,NULL,'Ballada jakich mało','url','maszynopis, na końcu z odręczną adnotacją','ZP_IZP_P_K2_5_0_002.TIFF','600 dpi TIFF, 4950x7008','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(85,NULL,'Skarb','url','maszynopis, z niewydanych wierszy i satyr','ZP_IZP_P_K2_6_0_001p.TIFF','600 dpi TIFF, 4950x7008','Monika Żytkowiak','brak','własność prywatna','public','1938-07-12','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(86,NULL,'Skarb','url','maszynopis, z niewydanych wierszy i satyr','ZP_IZP_P_K2_6_0_001.TIFF','600 dpi TIFF, 4950x7008','Brak informacji','brak','własność prywatna','public','1938-07-12','Brak informacji','Poznań','Brak informacji','Poezja',NULL,NULL,'Brak informacji','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(87,NULL,'Obiad','url','rękopis, w bardzo złym stanie, wybrakowany oraz maszynopis z przepisaną treścią, wraz z odręcznymi uwagami','ZP_IZP_P_T1_0_0_001p.TIFF','600 dpi TIFF, 3842x5229','Monika Żytkowiak','brak','własność prywatna','public','1974-08-10','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Jan Aleksander Fredro',NULL,NULL,NULL,NULL,''),(88,NULL,'Obiad','url','rękopis, w bardzo złym stanie, wybrakowany oraz maszynopis z przepisaną treścią, wraz z odręcznymi uwagami','ZP_IZP_P_T1_0_0_001.TIFF','600 dpi TIFF, 2257x5229','Monika Żytkowiak','brak','własność prywatna','public','1974-08-10','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Jan Aleksander Fredro',NULL,NULL,NULL,NULL,''),(89,NULL,'Obiad','url','rękopis, w bardzo złym stanie, wybrakowany oraz maszynopis z przepisaną treścią, wraz z odręcznymi uwagami','ZP_IZP_P_T1_0_0_002.TIFF','600 dpi TIFF, 2298x5229','Monika Żytkowiak','brak','własność prywatna','public','1974-08-10','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Jan Aleksander Fredro',NULL,NULL,NULL,NULL,''),(90,NULL,'Obiad','url','rękopis, w bardzo złym stanie, wybrakowany oraz maszynopis z przepisaną treścią, wraz z odręcznymi uwagami','ZP_IZP_P_T1_0_0_003.TIFF','600 dpi TIFF, 4950x6442','Monika Żytkowiak','brak','własność prywatna','public','1974-08-10','2014-07-29','Poznań','Epson Perfection V300 Photo','Poezja',NULL,NULL,'zbiory prywatne','poetry','Jan Aleksander Fredro',NULL,NULL,NULL,NULL,''),(91,NULL,'Do Bismarka','url','rękopis','ZP_ASZ_P_T1_0_0_001.TIFF','600 dpi TIFF, 5138x6502','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-10-30','Warszawa','Plustek OpticPro A320','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(92,NULL,'Do Bismarka','url','rękopis','ZP_ASZ_P_T1_0_0_002.TIFF','600 dpi TIFF, 5192x6474','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-10-30','Warszawa','Plustek OpticPro A320','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(93,NULL,'Do Bismarka','url','rękopis','ZP_ASZ_P_T1_0_0_003p.TIFF','600 dpi TIFF, 4206x4825','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-10-30','Warszawa','Plustek OpticPro A320','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,''),(94,NULL,'Do Bismarka','url','rękopis','ZP_ASZ_P_T1_0_0_003.TIFF','600 dpi TIFF, 3716x2758','Monika Żytkowiak','brak','własność prywatna','public','Brak informacji','2014-10-30','Warszawa','Plustek OpticPro A320','Poezja',NULL,NULL,'zbiory prywatne','poetry','Maria z Fredrów Szembekowa',NULL,NULL,NULL,NULL,'');
/*!40000 ALTER TABLE `archive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_category`
--

DROP TABLE IF EXISTS `archive_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_category`
--

LOCK TABLES `archive_category` WRITE;
/*!40000 ALTER TABLE `archive_category` DISABLE KEYS */;
INSERT INTO `archive_category` VALUES (1,'Mickiewicz','mickiewicz','All Mickiewicz\'s creations');
/*!40000 ALTER TABLE `archive_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fos_user_group`
--

DROP TABLE IF EXISTS `fos_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fos_user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_583D1F3E5E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fos_user_group`
--

LOCK TABLES `fos_user_group` WRITE;
/*!40000 ALTER TABLE `fos_user_group` DISABLE KEYS */;
INSERT INTO `fos_user_group` VALUES (1,'Użytkownicy','a:1:{i:0;s:9:\"ROLE_USER\";}'),(2,'Eksperci','a:2:{i:0;s:9:\"ROLE_USER\";i:1;s:11:\"ROLE_EXPERT\";}');
/*!40000 ALTER TABLE `fos_user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fos_user_user`
--

DROP TABLE IF EXISTS `fos_user_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fos_user_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `firstname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `biography` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `twitter_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `gplus_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `two_step_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_C560D76192FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_C560D761A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fos_user_user`
--

LOCK TABLES `fos_user_user` WRITE;
/*!40000 ALTER TABLE `fos_user_user` DISABLE KEYS */;
INSERT INTO `fos_user_user` VALUES (1,'admin','admin','admin@gmail.com','admin@gmail.com',1,'g0pbz215hdwkcosogokoswg44sw8sws','Bikw+wFlqgsJmgNw/7VcUuhYxEeaBCeUPNADXlsPlc08aX/AP3HJVnFiZ/07bKARF/xkoOnQXL2yvJRm/2Q9ww==','2014-11-27 22:11:24',0,0,NULL,NULL,NULL,'a:1:{i:0;s:10:\"ROLE_ADMIN\";}',0,NULL,'2014-11-03 21:12:37','2014-11-27 22:11:24',NULL,NULL,NULL,NULL,NULL,'u',NULL,NULL,NULL,NULL,NULL,'null',NULL,NULL,'null',NULL,NULL,'null',NULL,NULL),(2,'piotrek','piotrek','piotrek@gmail.com','piotrek@gmail.com',1,'80gaz6u0h3404kgw8oc8w4csockcc4c','YjvyjaGwcSK7Oou1Nil7SLNMcc0bUfFdMIh8SEa53iMISvQxGcH4eP0imQYdL1vrjaFfKuoWBvx5Qv9QVIBcdw==','2014-11-21 00:58:59',0,0,NULL,NULL,NULL,'a:1:{i:0;s:11:\"ROLE_EXPERT\";}',0,NULL,'2014-11-03 21:17:56','2014-11-21 00:58:59',NULL,'Piotr','Delkowski',NULL,NULL,'m',NULL,NULL,NULL,NULL,NULL,'null',NULL,NULL,'null',NULL,NULL,'null',NULL,NULL);
/*!40000 ALTER TABLE `fos_user_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fos_user_user_group`
--

DROP TABLE IF EXISTS `fos_user_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fos_user_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `IDX_B3C77447A76ED395` (`user_id`),
  KEY `IDX_B3C77447FE54D947` (`group_id`),
  CONSTRAINT `FK_B3C77447A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_B3C77447FE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_user_group` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fos_user_user_group`
--

LOCK TABLES `fos_user_user_group` WRITE;
/*!40000 ALTER TABLE `fos_user_user_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `fos_user_user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `import`
--

DROP TABLE IF EXISTS `import`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `import` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `import`
--

LOCK TABLES `import` WRITE;
/*!40000 ALTER TABLE `import` DISABLE KEYS */;
INSERT INTO `import` VALUES (5,'archive2.xls'),(6,'archive2.xls'),(7,'archive_546bda077aeea0.11678340'),(8,'61cf59ebd08be69adff208bd0ef25624xls'),(9,'archive_2099ab25507e86a1c61d8dac3422c535xls'),(10,'archive_62116bf02383da0578205ff13c22edd8.xls'),(11,'archive_9dfb2481405c0a861bd507e460aada66.xls'),(12,'archive_00a1fe3675ac0e49c75b480e13959e3c.xls'),(13,'archive_f342394a7757ce5e886f131d67689a81.xls'),(14,'archive_a485ab5c66f860634519a76114ebd604.xls'),(15,'archive_66660b8409c579b8e8294c768b78dd3f.xls'),(16,'archive_2f5d2d59a71813996e242caa875c5c02.xls'),(17,'archive_df3937afb501cdca7f8f86d8a69cd98a.xls'),(18,'archive_03ec7da37fa0c7eccc7490af29ac8f0a.xls'),(19,'archive_6958d0ac3a18e41052ff8fb503095399.xls'),(20,'archive_7b2796ae98ae496e6a0364ff7504bfa7.xls'),(21,'archive_fc7c876764c92e418971c8cf92f46dc0.xls'),(22,'archive_3561cd20c75c417382b42f7e1eec19a8.xls'),(23,'archive_647f01ab0d92b4ea182ad4c32aff8cd6.xls'),(24,'archive_6a04f5a74a643bd1808be1a1c349154d.xls'),(25,'archive_2b3ff10ffdde360c50141128d6277634.xls'),(26,'archive_8d9774df580833e8b3e5ef85aa73a23c.xls'),(27,'archive_21d7a1df000ed67cac01acd592460b42.xls'),(28,'archive_200b11f5d042214c909f979e6d3d471e.xls'),(29,'archive_70e32aa9c330efa4b43ac0ac0bd5814a.xls'),(30,'archive_9475a63e3df32f4b6f2fd02cf791ef8c.xls'),(31,'archive_576e0079fb62e27311e3371513f47204.xls'),(32,'archive_5fb6a856286f9eaa5d1455de0bcd7082.xls'),(33,'archive_24df32bfc3de1ba5d6af09e4547dd5e7.xls'),(34,'archive_8df37933967e92851738d992a52f897a.xls'),(35,'archive_74f509e7d007182b2058d03c3a9457bf.xls'),(36,'archive_72e7decccc4de08d507b4fd376db23a8.xls'),(37,'archive_7dfcc524fa48cfa8a56d8ec2b26c7240.xls'),(38,'archive_6a4cea43afdb6ea2a8fd2e0206933485.xls'),(39,'archive_d62234630c1b0ece3c00164904346453.xls'),(40,'archive_01537ae5ac017f44fd249de5833a468f.xls'),(41,'archive_eb11e039a1099312bd921f2ac541b7fa.xls'),(42,'archive_867b07a4b6fda884f76c7e3aa274f828.xls'),(43,'archive_56e5c76f10d62e7c4edf555a6115ab64.xls'),(44,'archive_8d81504d2fbcb292814f746e42fae5fa.xls'),(45,'archive_d5f3e63f7bf234d747110d45b6e64ebf.xls'),(46,'archive_322f151491b21ff3179ce2cf86b3be50.xls'),(47,'archive_d3f2f1a90ae501c928c9aa73e0966435.xls'),(48,'archive_80f0fb09b771e5ed3a1182e735383153.xls'),(49,'archive_9864f64c38da67162784b3afa484f6b3.xls');
/*!40000 ALTER TABLE `import` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `speciality` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` VALUES (1,'Monika','Żytkowiak','Tworzenie kopii cyfrowych','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed egestas mauris elit, ac imperdiet leo ornare nec. Aliquam vitae velit porta, interdum nisi nec, condimentum turpis. Nullam lacinia urna eget porttitor finibus. Quisque urna purus, lobortis ac consectetur eget, mollis sit amet enim. Integer eleifend mauris eros, quis pretium ex dignissim vel. Morbi pulvinar ipsum ut interdum consectetur. Mauris vestibulum at magna dictum pretium. Nulla dapibus pellentesque blandit. Etiam luctus velit eget libero pellentesque, tincidunt placerat mauris fringilla. Vivamus nec consectetur felis, eu bibendum arcu. Donec massa nisl, rutrum sed euismod in, pharetra rutrum risus. Sed sagittis orci et dolor rhoncus, non egestas dolor accumsan. Donec est nunc, ultrices sit amet libero non, tincidunt iaculis ipsum. Aenean metus est, vehicula vel blandit sit amet, semper a velit.','team_f3cfeb3c850e1b9e23fb0f85a4eb396d.jpg','2014-11-21'),(3,'Jan','Kowalski','CEO','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed egestas mauris elit, ac imperdiet leo ornare nec. Aliquam vitae velit porta, interdum nisi nec, condimentum turpis. Nullam lacinia urna eget porttitor finibus. Quisque urna purus, lobortis ac consectetur eget, mollis sit amet enim. Integer eleifend mauris eros, quis pretium ex dignissim vel. Morbi pulvinar ipsum ut interdum consectetur. Mauris vestibulum at magna dictum pretium. Nulla dapibus pellentesque blandit. Etiam luctus velit eget libero pellentesque, tincidunt placerat mauris fringilla. Vivamus nec consectetur felis, eu bibendum arcu. Donec massa nisl, rutrum sed euismod in, pharetra rutrum risus. Sed sagittis orci et dolor rhoncus, non egestas dolor accumsan. Donec est nunc, ultrices sit amet libero non, tincidunt iaculis ipsum. Aenean metus est, vehicula vel blandit sit amet, semper a velit.','team_c9e9aa5a16f56c591b37f675d0bdd45e.jpg','2014-11-21'),(4,'Katarzyna','Nowak','Sprzątaczka','There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.','team_aa2a39a15d64d24f2f9538ff15656dd9.jpg','2014-11-21');
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-28  1:31:20
