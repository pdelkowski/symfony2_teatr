-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 01, 2014 at 11:39 AM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `teatr`
--

-- --------------------------------------------------------

--
-- Table structure for table `static_page`
--

CREATE TABLE IF NOT EXISTS `static_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `static_page`
--

INSERT INTO `static_page` (`id`, `title`, `description`, `label`) VALUES
(1, 'Cyfrowe Archiwum i Muzeum Aleksandra Fredry', 'Lorem ipsum dolor sit amet consectetur adipiscing elit. Etiam placerat erat non massa porta consectetur. Nulla blandit dapibus tortor, ac auctor sapien vehicula et. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus tempus tellus non imperdiet mollis. Donec nec egestas quam, a tempus nisi. Cras non ex congue, tempor turpis vitae, finibus nulla. Suspendisse elit purus, laoreet at venenatis ut, dictum a erat. Integer maximus lectus id sapien ullamcorper, vitae gravida quam sagittis. Quisque sed turpis blandit, malesuada nulla nec, sodales odio. Interdum et malesuada fames ac ante ipsum primis in faucibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.\r\n\r\nVestibulum ac sapien eu felis consectetur condimentum. Pellentesque gravida posuere nisi. Vestibulum et porta tortor. Vivamus pulvinar egestas leo non varius. Fusce ultricies tempor arcu, sit amet feugiat nunc blandit sed. Maecenas hendrerit sem et est molestie, vel blandit dolor egestas. Pellentesque bibendum mi quis ipsum lobortis euismod.\r\n\r\nNulla efficitur quam eget molestie bibendum. Curabitur blandit massa eu est feugiat, id venenatis elit sodales. Proin fermentum justo id mauris accumsan semper. Suspendisse aliquam ligula lacus. Nullam ante orci, interdum eu diam vitae, pretium molestie orci. Nam semper, turpis sed vulputate mattis, nibh diam scelerisque enim, non auctor magna metus a augue. Praesent ullamcorper sed lectus hendrerit hendrerit. Duis ultrices diam quis viverra malesuada. Vestibulum dignissim, nulla eget fermentum feugiat, ante justo ultricies massa, eget gravida libero diam at mauris. Suspendisse posuere lacus quis diam maximus posuere. Donec vitae blandit erat, vitae porta augue. Donec purus arcu, lacinia nec lacinia eget, congue ac ipsum.\r\n\r\nSed porta accumsan erat. Ut blandit scelerisque commodo. Phasellus eu felis nec ligula placerat suscipit. In eleifend eu neque eu rutrum. Nam aliquam, risus id dapibus sodales, felis velit feugiat massa, vel aliquet magna mi quis elit. Praesent non nisl nunc. Proin in sollicitudin dui. Aenean at porta est. Aliquam ac leo facilisis, rutrum sem in, ullamcorper velit.\r\n\r\nAliquam erat volutpat. Donec malesuada tristique ligula, et efficitur magna suscipit iaculis. Aenean consequat finibus sem ut accumsan. Nullam ac ante maximus, interdum nisl in, tempus velit. Nunc finibus tincidunt placerat. Nulla vehicula iaculis suscipit. Nam pellentesque odio at sapien laoreet eleifend. Proin luctus purus sapien, ut rutrum odio consequat quis. Vestibulum viverra dignissim ipsum placerat convallis. Sed finibus vel risus sit amet gravida. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras non turpis quis velit lacinia varius a ut eros. Pellentesque egestas quam erat, sed bibendum diam vehicula ac. Pellentesque cursus ultrices risus sed dictum. Maecenas eu nulla eros.', 'Strona główna');
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
